/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID BASICSHOT = 4215250317U;
        static const AkUniqueID BIGSHOT = 3427604237U;
        static const AkUniqueID ENEMYHIT = 391172934U;
        static const AkUniqueID GAMEOVER = 4158285989U;
        static const AkUniqueID GOTOMENU = 1893297113U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID MUTEGAME = 1287522420U;
        static const AkUniqueID PLAYERHIT = 3831688773U;
        static const AkUniqueID PLAYERON1HP = 3395060318U;
        static const AkUniqueID PLAYERON2HP = 3562689317U;
        static const AkUniqueID PLAYERSON3HP = 2577391249U;
        static const AkUniqueID PLAYMENUTHEME = 367021569U;
        static const AkUniqueID POWERUP = 3950429679U;
        static const AkUniqueID SHIELDHIT = 2513197477U;
        static const AkUniqueID SNIPERSHOT = 1104796412U;
        static const AkUniqueID STOPMENUTHEME = 4186217887U;
    } // namespace EVENTS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
