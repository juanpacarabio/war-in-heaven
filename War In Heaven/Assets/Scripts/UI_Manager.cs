using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class UI_Manager : MonoBehaviour
{
    public TMPro.TMP_Text PlayerXWins, WinShadow, Scoreboard1, Scoreboard2, P1_kills, P2_kills, P1_kills_shadow, P2_kills_shadow;
    public GameObject GameOverScreen, HuntGameOverScreen;
    public GameObject GameUI;
    public string winner;
    public bool BossIsDead;
    public GameObject ReplayButton;

    public static UI_Manager instance = null;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

    }

    private void Start()
    {
        if (SceneManager.GetActiveScene().name != "BOSS")
        {
            Scoreboard1.text = ScoreTracker.instance.Player1_Score.ToString();
            Scoreboard2.text = ScoreTracker.instance.Player2_Score.ToString();
        }
        HuntGameOverScreen.SetActive(false);
    }
    public void GameOver()
    {
        GameUI.SetActive(false);
        GameOverScreen.SetActive(true);
        EventSystem.current.SetSelectedGameObject(ReplayButton);
        if (winner == "Player 1")
        {
            PlayerXWins.color = new Color32(0, 100, 255, 255);
            WinShadow.color = new Color(133, 255, 126, 255);
            ScoreTracker.instance.Player1_Score++;
        }
        if (winner == "Player 2")
        {
            PlayerXWins.color = new Color32(255, 143, 0, 255);
            WinShadow.color = new Color(250, 155, 155, 255);
            ScoreTracker.instance.Player2_Score++;
        }
        PlayerXWins.text = winner + " Wins!";
        WinShadow.text = PlayerXWins.text;
    }

    public void HuntGameOver()
    {
        GameUI.SetActive(false);
        GameOverScreen.SetActive(true);
        HuntGameOverScreen.SetActive(false);
        EventSystem.current.SetSelectedGameObject(ReplayButton);
        if (winner == "Player 1")
        {
            PlayerXWins.color = new Color32(0, 100, 255, 255);
            WinShadow.color = new Color(133, 255, 126, 255);
            ScoreTracker.instance.Player1_Score++;
        }
        if (winner == "Player 2")
        {
            PlayerXWins.color = new Color32(255, 143, 0, 255);
            WinShadow.color = new Color(250, 155, 155, 255);
            ScoreTracker.instance.Player2_Score++;
        }
        if (winner == "No One")
        {
            PlayerXWins.color = new Color32(255, 255, 255, 255);
            WinShadow.color = new Color(255, 255, 255, 255);
        }
        PlayerXWins.text = winner + " Wins!";
        WinShadow.text = PlayerXWins.text;
        HuntGameOverScreen.SetActive(true);
        P1_kills.text = ScoreTracker.instance.P1_Kills.ToString();
        P2_kills.text = ScoreTracker.instance.P2_Kills.ToString();
        P1_kills_shadow.text = P1_kills.text;
        P2_kills_shadow.text = P2_kills.text;
    }

    public void BossGameOver()
    {
        GameUI.SetActive(false);
        GameOverScreen.SetActive(true);
        EventSystem.current.SetSelectedGameObject(ReplayButton);
        if (winner == "Humanity")
        {
            PlayerXWins.color = new Color32(0, 255, 0, 255);
            WinShadow.color = new Color(255, 255, 255, 255);
        }
        if (winner == "Evil")
        {
            PlayerXWins.color = new Color32(255, 0, 0, 255);
            WinShadow.color = new Color(255, 255, 255, 255);
        }
        PlayerXWins.text = winner + " Wins!";
        WinShadow.text = PlayerXWins.text;
    }
}
