using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public GameObject player1_explosion;
    public GameObject player2_explosion;
    public GameObject Enemy_Explosion;
    public bool GameIsOver = false;
    public AK.Wwise.Event GameOverEvent;
    public AK.Wwise.Event ReturnToMenu;
    public bool BossIsDead;
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

    }

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        if (ScoreTracker.instance.Sound == false)
        {
            MusicScript.instance.Music.Stop(MusicScript.instance.gameObject, 0);
        }
    }
    public void EndGame(string player)
    {
        GameIsOver = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        GameOverEvent.Post(gameObject);
        MusicScript.instance.Music.Stop(MusicScript.instance.gameObject, 0);
        GameObject instanciator = GameObject.Find("PowerUp Instantiator");
        instanciator.SetActive(false);
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject controlled in players)
        {
            controlled.GetComponent<PlayerControl>().canmoveGeneral = false;
            controlled.GetComponent<PlayerControl>().canShoot = false;
            controlled.GetComponent<PlayerControl>().canBeHit = false;
        }
        GameObject[] BlackHoles = GameObject.FindGameObjectsWithTag("Spawner");
        foreach (GameObject spawner in BlackHoles)
        {
            spawner.GetComponent<BlackHole>().StopAllCoroutines();
        }
        GameObject[] Projectiles = GameObject.FindGameObjectsWithTag("Projectile");
        foreach (GameObject projectile in Projectiles)
        {
            Destroy(projectile.gameObject);
        }
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject destroyable in enemies)
        {
            Destroy(destroyable);
        }
        GameObject[] powerUps = GameObject.FindGameObjectsWithTag("PowerUp");
        foreach (GameObject destroyable in powerUps)
        {
            Destroy(destroyable);
        }
        GameObject[] Allies = GameObject.FindGameObjectsWithTag("Ally");
        foreach(GameObject ally in Allies)
        {
            ally.GetComponent<AllyScript>().GetDestroyed();
        }
        if (SceneManager.GetActiveScene().name != "HUNT" && SceneManager.GetActiveScene().name != "BOSS")
        {
            StartCoroutine(Cheers(player));
            UI_Manager.instance.winner = player;
            UI_Manager.instance.GameOver();
        }else if (SceneManager.GetActiveScene().name == "HUNT")
        {

            if (ScoreTracker.instance.P1_Kills > ScoreTracker.instance.P2_Kills)
            {
                player = "Player 1";
            }
            if (ScoreTracker.instance.P2_Kills > ScoreTracker.instance.P1_Kills)
            {
                player = "Player 2";
            }
            if (ScoreTracker.instance.P2_Kills == ScoreTracker.instance.P1_Kills)
            {
                player = "No One";
            }
            StartCoroutine(Cheers(player));
            UI_Manager.instance.winner = player;
            UI_Manager.instance.HuntGameOver();
        }else if (SceneManager.GetActiveScene().name == "BOSS")
        {
            if (BossIsDead == true)
            {
                player = "Humanity";
            }else if (BossIsDead == false)
            {
                player = "Evil";
            }
            StartCoroutine(Cheers(player));
            UI_Manager.instance.winner = player;
            UI_Manager.instance.BossGameOver();
        }
    }
    public void Restart()
    {
        ScoreTracker.instance.P1_Kills = 0;
        ScoreTracker.instance.P2_Kills = 0;
        ReturnToMenu.Post(gameObject);
        if (SceneManager.GetActiveScene().name == "NORMAL")
        {
            SceneManager.LoadScene(1);
        }
        if (SceneManager.GetActiveScene().name == "HUNT")
        {
            SceneManager.LoadScene(2);
        }
        if (SceneManager.GetActiveScene().name == "WALLS")
        {
            SceneManager.LoadScene(3);
        }
        if (SceneManager.GetActiveScene().name == "BOSS")
        {
            SceneManager.LoadScene(4);
        }
    }

    public void MainMenu()
    {
        ReturnToMenu.Post(gameObject);
        if (ScoreTracker.instance.TrackBySession == false)
        {
            ScoreTracker.instance.Player1_Score = 0;
            ScoreTracker.instance.Player2_Score = 0;
        }
        ScoreTracker.instance.P1_Kills = 0;
        ScoreTracker.instance.P2_Kills = 0;
        SceneManager.LoadScene(0);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
    GameObject fireworks;
    public IEnumerator Cheers(string player)
    { 
        bool defeat = false;
        if (player == "Player 1")
        {
            fireworks = player1_explosion;
        }
        if (player == "Player 2")
        {
            fireworks = player2_explosion;
        }
        if (player == "No One")
        {
            StopAllCoroutines();
        }
        if (player == "Humanity")
        {
            fireworks = player1_explosion;
        }
        if (player == "Evil")
        {
            if (Enemy_Explosion != null)
            {
                fireworks = Enemy_Explosion;
            }
            defeat = true;
        }

        bool a = true;
        while (a == true)
        {
            if (defeat == false && player == "Humanity")
            {
                if (fireworks == player1_explosion)
                {
                    fireworks = player2_explosion;
                }else if (fireworks == player2_explosion)
                {
                    fireworks = player1_explosion;
                }
            }

            Vector3 position = new Vector3(Random.Range(-10, 10), Random.Range(-3, 5), -7f);
            Quaternion rotation = new Quaternion();
            GameObject firework = Instantiate(fireworks, position, rotation);
            yield return new WaitForSeconds(0.2f);
        }
    }
}
