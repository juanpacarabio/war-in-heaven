using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TripleShot : LaserScript
{
    public GameObject ShootingPointA;
    public GameObject ShootingPointB;
    public GameObject projectile;
    public override void Start()
    {
        GameObject laserA = Instantiate(projectile, ShootingPointA.transform.position, ShootingPointA.transform.rotation);
        laserA.GetComponent<LaserScript>().player = player;
        GameObject laserB = Instantiate(projectile, ShootingPointB.transform.position, ShootingPointB.transform.rotation);
        laserB.GetComponent<LaserScript>().player = player;
        if (player.gameObject.name == "Player1")
        {
            Quaternion rotationA = Quaternion.Euler(0f, 0f, 45f);
            //transform.Rotate(0f, 0f, 180f);
            speed = 10;
            laserA.transform.Rotate(0f, 0f, 45f, Space.World);
            laserA.GetComponent<LaserScript>().speed = laserA.GetComponent<LaserScript>().speed;
            laserB.transform.Rotate(0f, 0f, -45f, Space.World);
            laserB.GetComponent<LaserScript>().speed = laserB.GetComponent<LaserScript>().speed;
        }

        if (player.gameObject.name == "Player2")
        {
            laserA.transform.Rotate(0f, 0f, -45f, Space.World);
            laserA.GetComponent<LaserScript>().speed = laserA.GetComponent<LaserScript>().speed;
            laserB.transform.Rotate(0f, 0f, 45f, Space.World);
            laserB.GetComponent<LaserScript>().speed = laserB.GetComponent<LaserScript>().speed;
        }
    }


    
}
