using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : LaserScript
{
    public float growSpeed;
    Vector3 scaleSpeed;
    public float MaxSize;
    bool growing;
    public override void Start()
    {
        base.Start();
        transform.parent = player.transform;
        scaleSpeed = new Vector3(0f, growSpeed, 0f);
        growing = true;
    }

    public override void Update()
    {

        if (growing == true)
        {
            transform.localScale = transform.localScale + scaleSpeed * Time.deltaTime;
            if (transform.localScale.y >= MaxSize)
            {
                growing = false;
            }
        }
        if (growing == false)
        {
            transform.localScale = transform.localScale - scaleSpeed * Time.deltaTime;
            if (transform.localScale.y <= 1)
            {
                Destroy(this.gameObject);
            }
        }
    }

}
