using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BOSSAmmo : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerControl>().TakeDamage();
            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("Ally"))
        {
            collision.gameObject.GetComponent<AllyScript>().GetDestroyed();
            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("PowerUp"))
        {
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
    }
}
