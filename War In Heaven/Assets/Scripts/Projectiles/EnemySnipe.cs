using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySnipe : EnemyLaser
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.name == "Player1" && origin.GetComponent<ShootingEnemy>().allyplayer1 == false)
            {
                collision.gameObject.GetComponent<PlayerControl>().TakeDamage();
                Destroy(this.gameObject);
            }
            if (collision.gameObject.name == "Player2" && origin.GetComponent<ShootingEnemy>().allyplayer2 == false)
            {
                collision.gameObject.GetComponent<PlayerControl>().TakeDamage();
                Destroy(this.gameObject);
            }
        }
    }
}
