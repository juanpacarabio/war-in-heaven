using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour
{
    public GameObject player;
    public float speed;

    public virtual void Start()
    {
        speed = player.GetComponent<PlayerControl>().WeaponSpeed;
    }

    public virtual void Update()
    {
        transform.Translate(Time.deltaTime * speed, 0f, 0f) ;
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            bool isGrower = false;
            Grower script = collision.gameObject.GetComponent<Grower>();
            if (script != null)
            {
                isGrower = true;
            }
            if (isGrower == true)
            {
                Destroy(this.gameObject);
            }
            bool isBoss = false;
            BOSSGrower bosscheck = collision.gameObject.GetComponent<BOSSGrower>();
            if (bosscheck != null)
            {
                isBoss = true;
            }
            if (isBoss == true)
            {
                Destroy(this.gameObject);
            }
            
            if (isGrower == false && isBoss == false)
            {
                Destroy(collision.gameObject);
            }

            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.name == "Player1" && collision.gameObject != player)
            {
                collision.gameObject.GetComponent<Player1>().TakeDamage();
            }
            if (collision.gameObject.name == "Player2" && collision.gameObject != player)
            {
                collision.gameObject.GetComponent<Player2>().TakeDamage();
            }

            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("Ally"))
        {
            collision.gameObject.GetComponent<AllyScript>().GetDestroyed();
            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("BOSS"))
        {
            collision.gameObject.GetComponent<BossController>().TakeDamage(1);
            Destroy(this.gameObject);
        }
    }
}
