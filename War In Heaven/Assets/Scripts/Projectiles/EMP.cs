using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EMP : LaserScript
{
    public override void OnCollisionEnter(Collision collision)
    {
       
        if (collision.gameObject.CompareTag("Player") && collision.gameObject != player)
        {
                collision.gameObject.GetComponent<PlayerControl>().EMPhit();
                Destroy(this.gameObject);
        }

    }
}
