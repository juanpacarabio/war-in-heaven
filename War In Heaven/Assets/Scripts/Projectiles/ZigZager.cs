using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZigZager : LaserScript
{
    public float baseTimeToSwitch = 0.005f;
    float TimeToSwitch;
    public bool GoingUp = true;
    public override void Start()
    {
        base.Start();
        TimeToSwitch = baseTimeToSwitch;
        speed = 4f;
        if (player.GetComponent<PlayerControl>().name == "Player2")
        {
            speed = speed * -1;
        }
    }
    public override void Update()
    {
        base.Update();
        if (GoingUp == true)
        {
            transform.Translate(0f, Time.deltaTime * 4, 0f);
        }
        if (GoingUp == false)
        {
            transform.Translate(0f, -Time.deltaTime * 4, 0f);
        }

        TimeToSwitch = TimeToSwitch - Time.deltaTime;
        if (TimeToSwitch <= 0)
        {
            GoingUp = !GoingUp;
            TimeToSwitch = baseTimeToSwitch;
        }
    }
}
