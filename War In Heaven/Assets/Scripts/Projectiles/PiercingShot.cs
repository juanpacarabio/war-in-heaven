using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiercingShot : LaserScript
{
    public override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") )
        {
            if (collision.gameObject.name == "Player1" && collision.gameObject != player)
            {
                collision.gameObject.GetComponent<Player1>().TakeDamage();
            }
            if (collision.gameObject.name == "Player2" && collision.gameObject != player)
            {
                collision.gameObject.GetComponent<Player2>().TakeDamage();
            }
            if (collision.gameObject.CompareTag("BOSS"))
            {
                collision.gameObject.GetComponent<BossController>().TakeDamage(1);
                Destroy(this.gameObject);
            }

            Destroy(this.gameObject);
        }
    }
}
