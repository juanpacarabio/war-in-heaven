using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLaser : MonoBehaviour
{
    public float speed;
    public GameObject origin;
    void Start()
    {
        if (origin.GetComponent<ShootingEnemy>().allyplayer1 == false && origin.GetComponent<ShootingEnemy>().allyplayer2 == false)
        {
            if (origin.GetComponent<ShootingEnemy>().side == 0)
            {
                speed = -10f;
            }
            if (origin.GetComponent<ShootingEnemy>().side == 1)
            {
                speed = -10f;
            }
        }
        if (origin.GetComponent<ShootingEnemy>().allyplayer1 == true)
        {
            speed = -10f;
        }
        if (origin.GetComponent<ShootingEnemy>().allyplayer2 == true)
        {
            speed = -10f;
        }
    }

    void Update()
    {
        transform.Translate(Time.deltaTime * speed, 0f, 0f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject != origin)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                if (collision.gameObject.name == "Player1" && origin.GetComponent<ShootingEnemy>().allyplayer1 == false)
                {
                    collision.gameObject.GetComponent<PlayerControl>().TakeDamage();
                    Destroy(this.gameObject);
                }
                if (collision.gameObject.name == "Player2" && origin.GetComponent<ShootingEnemy>().allyplayer2 == false)
                {
                    collision.gameObject.GetComponent<PlayerControl>().TakeDamage();
                    Destroy(this.gameObject);
                }
            }
            if (collision.gameObject.CompareTag("Projectile"))
            {
                bool isGrower = false;
                Grower script = collision.gameObject.GetComponent<Grower>();
                if (script != null)
                {
                    isGrower = true;
                }
                if (isGrower == true)
                {
                    Destroy(this.gameObject);
                }
                if (isGrower == false)
                {
                    Destroy(collision.gameObject);
                }
                if (collision.gameObject.GetComponent<LaserScript>().player.gameObject.name == "Player1" && origin.GetComponent<ShootingEnemy>().allyplayer1 == false)
                {
                    Destroy(collision.gameObject);
                    Destroy(this.gameObject);
                }
                if (collision.gameObject.GetComponent<LaserScript>().player.gameObject.name == "Player2" && origin.GetComponent<ShootingEnemy>().allyplayer2 == false)
                {
                    Destroy(collision.gameObject);
                    Destroy(this.gameObject);
                }
            }
            if (collision.gameObject.CompareTag("Enemy") && collision.gameObject != origin)
            {
                collision.gameObject.GetComponent<BaseEnemy>().Hit();
                Destroy(this.gameObject);
            }
            if (collision.gameObject.CompareTag("Ally"))
            {
                collision.gameObject.GetComponent<AllyScript>().GetDestroyed();
            }
        }
    }
}
