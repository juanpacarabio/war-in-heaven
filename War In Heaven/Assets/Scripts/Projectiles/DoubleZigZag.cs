using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleZigZag : LaserScript
{
    public GameObject Top, Bot;
    public GameObject projectile;

    public override void Start()
    {
        GameObject BallA = Instantiate(projectile, Top.transform.position, Top.transform.rotation);
        GameObject BallB = Instantiate(projectile, Bot.transform.position, Bot.transform.rotation);
        BallA.GetComponent<ZigZager>().player = player;
        BallB.GetComponent<ZigZager>().player = player;
        BallB.GetComponent<ZigZager>().GoingUp = false;
        Destroy(this);
    }
}
