using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grower : LaserScript
{
    public float growSpeed;
    Vector3 scaleSpeed;
    public override void Start()
    {
        base.Start();
        speed = 5;
        if (player.name == "Player1")
        {
            speed = 5;
        }else if (player.name == "Player2")
        {
            speed = -5;
        }

        scaleSpeed = new Vector3(growSpeed, growSpeed, growSpeed);
    }
    public override void Update()
    {
        base.Update();
        transform.localScale = transform.localScale + scaleSpeed * Time.deltaTime;
    }

    public override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.name == "Player1" && collision.gameObject != player)
            {
                collision.gameObject.GetComponent<Player1>().TakeDamage();
            }
            if (collision.gameObject.name == "Player2" && collision.gameObject != player)
            {
                collision.gameObject.GetComponent<Player2>().TakeDamage();
            }
            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("PowerUp"))
        {
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("BOSS"))
        {
            collision.gameObject.GetComponent<BossController>().TakeDamage(1);
            Destroy(this.gameObject);
        }
    }
}
