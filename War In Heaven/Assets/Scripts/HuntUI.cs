using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HuntUI : MonoBehaviour
{
    public static HuntUI instance = null;
    public TMPro.TMP_Text P1_kills, P2_kills;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        UpdateScoreboard();
    }
    public void UpdateScoreboard()
    {
        P1_kills.text = ScoreTracker.instance.P1_Kills.ToString();
        P2_kills.text = ScoreTracker.instance.P2_Kills.ToString();
    }

}
