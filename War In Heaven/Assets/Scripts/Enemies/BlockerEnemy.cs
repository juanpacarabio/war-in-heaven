using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockerEnemy : BaseEnemy
{
    float movingTime;

    public override void Start()
    {
        if (transform.position.x < 0)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0f);
            side = 0;
        }
        if (transform.position.x > 0)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0f);
            side = 1;
        }
        GameObject player = GameObject.Find("Player1");
        transform.position = new Vector3(transform.position.x, transform.position.y, player.transform.position.z);
        verticalMovement = Random.Range(-2, 3);
        switchVertical = baseSwitchVertical;
        movingTime = Random.Range(2, 4);
        hp = 3;
    }

    public override void Update()
    {
        base.Update();
        if (movingTime > 0)
        {
            movingTime = movingTime - Time.deltaTime;
        }
    }

    public override void Move()
    {
        if (movingTime > 0)
        {
            base.Move();
        }
    }

    public override void Verticality()
    {
        verticalMovement = Random.Range(-2, 3);
        switchVertical = baseSwitchVertical;
    }

    public override void Hit()
    {
        base.Hit();
        parts[hp].gameObject.SetActive(false);
    }

    public override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.name == "Player1" && allyplayer1 == false)
            {
                collision.gameObject.GetComponent<PlayerControl>().TakeDamage();
                Destroy(this.gameObject);
            }
            if (collision.gameObject.name == "Player2" && allyplayer2 == false)
            {
                collision.gameObject.GetComponent<PlayerControl>().TakeDamage();
                Destroy(this.gameObject);
            }
        }

        if (collision.gameObject.CompareTag("Projectile") && collision.gameObject.name.Substring(0, 3) != "EMP")
        {
            if (collision.gameObject.GetComponent<LaserScript>().player.gameObject.name == "Player1" && allyplayer1 == false)
            {
                lasthit = collision.gameObject.GetComponent<LaserScript>().player.gameObject;
                Destroy(collision.gameObject);
                Hit();
            }
            if (collision.gameObject.GetComponent<LaserScript>().player.gameObject.name == "Player2" && allyplayer2 == false)
            {
                lasthit = collision.gameObject.GetComponent<LaserScript>().player.gameObject;
                Destroy(collision.gameObject);
                Hit();
            }
        }
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(collision.gameObject);
            Hit();
        }
    }
}
