using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingEnemy : BaseEnemy
{
    public float fireRate;
    public GameObject instancePointA;
    public GameObject instancePointB;
    public GameObject projectile;
    public override void Start()
    {
        base.Start();
        StartCoroutine(Shoot());
    }
    bool shooting = true;
    public IEnumerator Shoot()
    {
        while (shooting == true)
        {
            yield return new WaitForSeconds(fireRate);
            GameObject laserA = Instantiate(projectile, instancePointA.transform.position, instancePointA.transform.rotation);
            laserA.GetComponent<EnemyLaser>().origin = this.gameObject;
            if (instancePointB != null)
            {
                GameObject laserB = Instantiate(projectile, instancePointB.transform.position, instancePointB.transform.rotation);
                laserB.GetComponent<EnemyLaser>().origin = this.gameObject;
            }
        }
    }
}
