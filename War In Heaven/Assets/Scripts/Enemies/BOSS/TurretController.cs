using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour
{
    public GameObject ShootingPoint;
    public bool IsActive;
    public GameObject CurrentProjectile;
    public GameObject[] Projectiles;
    public float FireRate;
    public float MinFireRate, MaxFireRate;


    #region ShootingDirection
    public bool ShootsLeft, ShootsRight, ShootsUp, ShootsDown;
    #endregion
    void Start()
    {
        IsActive = true;
        StartCoroutine(Shoot());
    }


    void Update()
    {
        if (IsActive == true)
        {
            transform.localEulerAngles = new Vector3(0, 0, Mathf.PingPong(Time.time * 60, 60));
        }    
    }
    public void TurnOff()
    {
        IsActive = false;
        StopCoroutine(Shoot());
    }
    public void TurnOn()
    {
        IsActive = true;
        StartCoroutine (Shoot());
    }
    public IEnumerator Shoot()
    {
        yield return new WaitForSeconds(2f);
        bool a = true;
        while (a == true)
        {
            if (IsActive == true)
            {
                GameObject projectile;
                projectile = Instantiate(CurrentProjectile, ShootingPoint.transform.position, ShootingPoint.transform.rotation);

                Rigidbody body = projectile.GetComponent<Rigidbody>();

                if (ShootsLeft == true)
                {
                    body.AddForce(this.transform.right * 5 * -1, ForceMode.Impulse);
                }
                if (ShootsRight == true)
                {
                    body.AddForce(this.transform.right * 5, ForceMode.Impulse);
                }
                if (ShootsDown == true)
                {
                    body.AddForce(this.transform.up * 5 * -1, ForceMode.Impulse);
                }
                if (ShootsUp == true)
                {
                    body.AddForce(this.transform.up * 5, ForceMode.Impulse);
                }
                FireRate = Random.Range(MinFireRate, MaxFireRate);
            }
            yield return new WaitForSeconds(FireRate);
        }
    }
}
