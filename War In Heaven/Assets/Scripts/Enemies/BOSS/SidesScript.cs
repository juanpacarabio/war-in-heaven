using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SidesScript : MonoBehaviour
{
    public GameObject BOSS;
    public GameObject[] Lights;
    public GameObject[] Turrets;
    public GameObject Cannon;
    public int SideHP;
    public int BaseHP = 5;
    public bool IsActive;
    public float OfflineTime = 3f;

    bool CannonPhase, CrazyPhase;

    private void Start()
    {
        IsActive = true;
        CannonPhase = false;
        SideHP = BaseHP;
    }
    private void Update()
    {
        if (GameManager.instance.GameIsOver == true)
        {
            IsActive = false;
            foreach (GameObject obj in Turrets)
            {
                obj.GetComponent<TurretController>().IsActive = false;
            }
            Cannon.GetComponent<TurretController>().IsActive = false;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            if (IsActive == true)
            {
                TakeDamage();
            }
            Destroy(collision.gameObject);
        }

    }

    public void TakeDamage()
    {
        if (SideHP > 0)
        {
            Lights[SideHP-1].SetActive(false);
            SideHP--;
            if (SideHP <= 0)
            {
                StartCoroutine(GoOffline());
            }
            BOSS.GetComponent<BossController>().TakeDamage(1);
        }
        if (SideHP <= 0)
        {
            
        }
    }

    public void SwitchToCannon()
    {
        CannonPhase = true;
        foreach (GameObject obj in Turrets)
        {
            obj.GetComponent<TurretController>().TurnOff();
        }
        Cannon.GetComponent<TurretController>().TurnOn();
        CannonPhase = true;
    }

    public void SwitchToTurrets()
    {
        CannonPhase = false;
        foreach (GameObject obj in Turrets)
        {
            obj.GetComponent<TurretController>().TurnOn();
        }
        Cannon.GetComponent <TurretController>().TurnOff();
        CannonPhase = false;
    }

    public void GoCrazy()
    {
        CrazyPhase = true;
        CannonPhase = false;
        foreach (GameObject obj in Turrets)
        {
            obj.GetComponent<TurretController>().TurnOn();
        }
        Cannon.GetComponent<TurretController>().TurnOn();
    }
    public IEnumerator GoOffline()
    {
        IsActive = false;
        BOSS.GetComponent<BossController>().TakeDamage(5);
        foreach (GameObject obj in Turrets)
        {
            obj.GetComponent<TurretController>().TurnOff();
        }
        Cannon.GetComponent<TurretController>().TurnOff();
        yield return new WaitForSeconds(OfflineTime);
        if (CannonPhase == true)
        {
            Cannon.GetComponent<TurretController>().TurnOn();
        }
        if (CannonPhase == false)
        {
            foreach (GameObject obj in Turrets)
            {
                obj.GetComponent<TurretController>().TurnOn();
            }
        }
        if (CrazyPhase == true)
        {
            foreach (GameObject obj in Turrets)
            {
                obj.GetComponent<TurretController>().TurnOn();
            }
            Cannon.GetComponent<TurretController>().TurnOn();
        }
        foreach (GameObject obj in Lights)
        {
            obj.SetActive(true);
        }
        IsActive = true;
        SideHP = BaseHP;
    }
}
