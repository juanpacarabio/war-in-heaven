using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterScript : MonoBehaviour
{
    public bool Shooting;
    public int ShootingSide;
    public GameObject LeftSide, RightSide;
    public GameObject CurrentProjectile;
    float FireRate;
    public GameObject BOSS;
    private void Start()
    {
        Shooting = true;
        StartCoroutine(Shoot());
    }
    private void Update()
    {
        if (GameManager.instance.GameIsOver == true)
        {
            Shooting = false;
        }
    }
    public void CannonMode()
    {
        StopCoroutine(Shoot());
        Shooting = false;
    }
    public void StartShooting()
    {
        StartCoroutine(Shoot());
        Shooting = true;
    }
    public IEnumerator Shoot()
    {
        yield return new WaitForSeconds(5);

        bool a = true;
        while (a == true)
        {
            if (Shooting == true)
            {
                ShootingSide = Random.Range(0, 11);
                if (ShootingSide <= 5)
                {
                    GameObject projectile;
                    projectile = Instantiate(CurrentProjectile, LeftSide.transform.position, LeftSide.transform.rotation);

                    Rigidbody body = projectile.GetComponent<Rigidbody>();
                    body.AddForce(this.transform.right * 5 * -1, ForceMode.Impulse);
                }
                if (ShootingSide > 5)
                {
                    GameObject projectile;
                    projectile = Instantiate(CurrentProjectile, RightSide.transform.position, RightSide.transform.rotation);

                    Rigidbody body = projectile.GetComponent<Rigidbody>();
                    body.AddForce(this.transform.right * 5, ForceMode.Impulse);
                }
                FireRate = Random.Range(1, 5);
                ShootingSide = Random.Range(0, 11);
            }
            yield return new WaitForSeconds(FireRate);
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Destroy(collision.gameObject);
            BOSS.GetComponent<BossController>().TakeDamage(3);
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerControl>().TakeDamage();
            BOSS.GetComponent<BossController>().TakeDamage(3);
        }
        if (collision.gameObject.CompareTag("Ally"))
        {
            collision.gameObject.GetComponent<AllyScript>().GetDestroyed();
        }
        if (collision.gameObject.CompareTag("PowerUp"))
        {
            Destroy(collision.gameObject);
        }
    }
}
