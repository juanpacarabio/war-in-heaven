using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    #region PARTS
    public GameObject[] Turrets;
    public GameObject[] Cannons;
    public GameObject Eye;
    public GameObject Top, Bottom;
    public GameObject BOSS;
    #endregion

    public TMPro.TMP_Text UI_Health;
    public int BossHealth;
    bool IsCrazy, CannonStage, TurretStage;
    bool RotatingToHorizontal;
    bool RotatingToVertical;
    private void Start()
    {
        TurretPhase();
        BossHealth = 100;
        UI_Health.text = BossHealth.ToString();
    }

    private void Update()
    {
        
        if (IsCrazy == true)
        {
            transform.Rotate(0f, 0f, 100f * Time.deltaTime);
        }
        if (RotatingToHorizontal == true)
        {
            transform.Rotate(0f, 0f, 10f * Time.deltaTime);
            if (transform.rotation.eulerAngles.z > 90f && transform.rotation.eulerAngles.z < 91f)
            {
                RotatingToHorizontal = false;
            }
        }
        if (RotatingToVertical == true)
        {
            transform.Rotate(0f, 0f, -10f * Time.deltaTime);
            if (transform.rotation.eulerAngles.z > 0f && transform.rotation.eulerAngles.z < 1f)
            {
                RotatingToVertical = false;
            }
        }
    }

    public void TakeDamage(int DamageAmount)
    {
        BossHealth = BossHealth - DamageAmount;
        UI_Health.text = BossHealth.ToString();
        if (BossHealth < 60 && TurretStage == true)
        {
            CannonPhase();
        }
        if (BossHealth < 30 && CannonStage == true)
        {
            LastStand();
        }
        if (BossHealth <= 0)
        {
            UI_Health.text = "0";
            GameManager.instance.BossIsDead = true;
            GameManager.instance.EndGame("Humanity");
        }
    }
    public void TurretPhase()
    {
        TurretStage = true;
        CannonStage = false;
        IsCrazy = false;
        Eye.GetComponent<CenterScript>().StartShooting();
        Top.GetComponent<SidesScript>().SwitchToTurrets();
        Bottom.GetComponent<SidesScript>().SwitchToTurrets();
    }

    public void CannonPhase()
    {
        TurretStage = false;
        CannonStage = true;
        RotatingToHorizontal = true;
        RotatingToVertical = false;
        Eye.GetComponent<CenterScript>().CannonMode();
        Top.GetComponent<SidesScript>().SwitchToCannon();
        Bottom.GetComponent<SidesScript>().SwitchToCannon();
    }

    public void LastStand()
    { 
        CannonStage = false;
        RotatingToHorizontal = false;
        RotatingToVertical = false;
        IsCrazy = true;
        Eye.GetComponent<CenterScript>().StartShooting();
        Top.GetComponent <SidesScript>().GoCrazy();
        Bottom.GetComponent <SidesScript>().GoCrazy();
    }
}
