using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnipingEnemy : ShootingEnemy
{
    public override void Move()
    {
        if (allyplayer1 == false && allyplayer2 == false)
        {
            if (side == 0)
            {
                transform.Translate(Time.deltaTime/2, Time.deltaTime * verticalMovement, 0f, Space.World);
            }
            if (side == 1)
            {
                transform.Translate(-Time.deltaTime/2, Time.deltaTime * verticalMovement, 0f, Space.World);
            }
        }
        if (allyplayer1 == true)
        {
            transform.Translate(Time.deltaTime/2, Time.deltaTime * verticalMovement, 0f, Space.World);
        }
        if (allyplayer2 == true)
        {
            transform.Translate(-Time.deltaTime/2, Time.deltaTime * verticalMovement, 0f, Space.World);
        }
    }
}
