using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BaseEnemy : MonoBehaviour
{
    public int hp;

    public int side; // 0 - left, 1 - right, 2 - centre
    public float BaseMovingTime;
    internal float switchVertical;
    public float baseSwitchVertical;
    public GameObject explosion;
    public GameObject lasthit;
    public AK.Wwise.Event HitSound;
    public virtual void Start()
    {
        if (transform.position.x < 0)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, -90f);
            side = 0;
        }
        if (transform.position.x > 0)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 90f);
            side = 1;
        }
        GameObject player = GameObject.Find("Player1");
        transform.position = new Vector3(transform.position.x, transform.position.y, player.transform.position.z);
        verticalMovement = Random.Range(-1, 2);
        switchVertical = baseSwitchVertical;
    }

    public float verticalMovement;
    public virtual void Update()
    {
        Move();

        if (switchVertical > 0)
        {
            switchVertical = switchVertical - Time.deltaTime;
        }
        if (switchVertical <= 0)
        {
            Verticality();
        }
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.name == "Player1" && allyplayer1 == false)
            {
                collision.gameObject.GetComponent<PlayerControl>().TakeDamage();
                Instantiate(explosion, transform.position, transform.rotation);
                ScoreTracker.instance.P1_Kills++;
                HitSound.Post(gameObject);
                Destroy(this.gameObject);
            }
            if (collision.gameObject.name == "Player2" && allyplayer2 == false)
            {
                collision.gameObject.GetComponent<PlayerControl>().TakeDamage();
                Instantiate(explosion, transform.position, transform.rotation);
                ScoreTracker.instance.P2_Kills++;
                HitSound.Post(gameObject);
                Destroy(this.gameObject);
            }
        }
        if (collision.gameObject.name.Substring(0,3) == "EMP")
        {
            EMPplayer = collision.gameObject.GetComponent<EMP>().player;
            SwitchSide();
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("Projectile") && collision.gameObject.name.Substring(0,3) != "EMP")
        {
            if (collision.gameObject.GetComponent<LaserScript>().player.gameObject.name == "Player1" && allyplayer1 == false)
            {
                lasthit = collision.gameObject.GetComponent<LaserScript>().player.gameObject;
                Destroy(collision.gameObject);
                Hit();
            }
            if (collision.gameObject.GetComponent<LaserScript>().player.gameObject.name == "Player2" && allyplayer2 == false)
            {
                lasthit = collision.gameObject.GetComponent<LaserScript>().player.gameObject;
                Destroy(collision.gameObject);
                Hit();
            }
        }
        if (collision.gameObject.CompareTag("Enemy"))
        {

            Destroy(collision.gameObject);
            Hit();
        }
        if (collision.gameObject.CompareTag("Ally"))
        {
            lasthit = collision.gameObject.GetComponent<AllyScript>().player.gameObject;
            collision.gameObject.GetComponent<AllyScript>().GetDestroyed();
            Hit();
        }
    }

    GameObject EMPplayer;
    public Material purple;
    public Material green;
    public Material red;
    public GameObject[] parts;
    public bool allyplayer1 = false;
    public bool allyplayer2 = false;
    public void SwitchSide()
    {
        Debug.Log(this.gameObject.name + " is now allied with " + EMPplayer.name);
        if (EMPplayer.name == "Player1")
        {
            foreach(GameObject piece in parts)
            {
                piece.GetComponent<Renderer>().material = green;
            }
            if (transform.position.x < 0)
            {
                if (allyplayer1 == false)
                {
                    transform.Rotate(transform.rotation.x, transform.rotation.y, 180f);
                }
            }
            allyplayer2 = false;
            allyplayer1 = true;
        }
        if (EMPplayer.name == "Player2")
        {
            foreach (GameObject piece in parts)
            {
                piece.GetComponent<Renderer>().material = red;
            }
            if (transform.position.x > 0)
            {
                if (allyplayer2 == false)
                {
                    transform.Rotate(transform.rotation.x, transform.rotation.y, 180f);
                }
            }
            allyplayer1 = false;
            allyplayer2 = true;
        }
    }
    public virtual void Move()
    {
        if (allyplayer1 == false && allyplayer2 == false)
        {
            if (side == 0)
            {
                transform.Translate(Time.deltaTime, Time.deltaTime * verticalMovement, 0f, Space.World);
            }
            if (side == 1)
            {
                transform.Translate(-Time.deltaTime, Time.deltaTime * verticalMovement, 0f, Space.World);
            }
        }
        if (allyplayer1 == true)
        {
            transform.Translate(Time.deltaTime, Time.deltaTime * verticalMovement, 0f, Space.World);
        }
        if (allyplayer2 == true)
        {
            transform.Translate(-Time.deltaTime, Time.deltaTime * verticalMovement, 0f, Space.World);
        }
    }
    public virtual void Verticality()
    {
        verticalMovement = Random.Range(-1, 2);
        switchVertical = baseSwitchVertical;
    }

    public virtual void Hit()
    {
        hp--;
        if (hp <= 0)
        {
            if (lasthit.gameObject.name == "Player1")
            {
                ScoreTracker.instance.P1_Kills++;
            }
            if (lasthit.gameObject.name == "Player2")
            {
                ScoreTracker.instance.P2_Kills++;
            }
            Instantiate(explosion, transform.position, transform.rotation);
            if (SceneManager.GetActiveScene().name == "HUNT")
            {
                HuntUI.instance.UpdateScoreboard();
            }
            else
            {

            }
            HitSound.Post(gameObject);
            Destroy(this.gameObject);
        }
    }
}
