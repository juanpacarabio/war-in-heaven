using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public GameObject menu;
    public GameObject SelectScreen;
    public GameObject credits;
    public GameObject options;
    public GameObject controls;
    public GameObject ButtonBySessionON, ButtonBySessionOFF;
    public GameObject SetMusicOFF, SetMusicON;

    public AK.Wwise.Event Music;
    public AK.Wwise.Event StopMusic;

    private void Start()
    {
        menu.SetActive(true);
        options.SetActive(false);
        credits.SetActive(false);
        SelectScreen.SetActive(false);

        if (ScoreTracker.instance.Sound == true)
        {
            Music.Post(gameObject);
        }
        
    }
    public GameObject NormalModeButton;
    public void StartGame()
    {
        menu.SetActive(false);
        SelectScreen.SetActive(true);
        EventSystem.current.SetSelectedGameObject(NormalModeButton);
    }

    public void PlayNormal()
    {
        
        StopMusic.Post(gameObject);
        SceneManager.LoadScene(1);
    }

    public void PlayHunt()
    {
        StopMusic.Post(gameObject);
        SceneManager.LoadScene(2);
    }

    public void PlayWalls()
    {
        StopMusic.Post(gameObject);
        SceneManager.LoadScene(3);
    }

    public void PlayCrazy()
    {
        StopMusic.Post(gameObject);
        SceneManager.LoadScene(4);
    }

    public GameObject PlayButton;
    public void Menu()
    {
        menu.SetActive(true);
        SelectScreen.SetActive(false);
        credits.SetActive(false);
        options.SetActive(false);
        EventSystem.current.SetSelectedGameObject(PlayButton);
    }

    public GameObject OptionsReturn;
    public void Options()
    {
        menu.SetActive(false);
        options.SetActive(true);
        EventSystem.current.SetSelectedGameObject(OptionsReturn);
        if (ScoreTracker.instance.TrackBySession == true)
        {
            ButtonBySessionON.SetActive(true);
            ButtonBySessionOFF.SetActive(false);
        }else if(ScoreTracker.instance.TrackBySession == false)
        {
            ButtonBySessionOFF.SetActive(true);
            ButtonBySessionON.SetActive(false);
        }

        if (ScoreTracker.instance.Sound == true)
        {
            SetMusicOFF.SetActive(true);
            SetMusicON.SetActive(false);
        }else if (ScoreTracker.instance.Sound == false)
        {
            SetMusicON.SetActive(true);
            SetMusicOFF.SetActive(false);
        }
    }
    public TMPro.TMP_Text TrackingText, TrackingShade;
    public void TrackBySessionON() //Is off - GAME bttn on
    {
        ButtonBySessionOFF.SetActive(false);
        ScoreTracker.instance.TrackBySession = true;
        ButtonBySessionON.SetActive(true);
        TrackingText.text = "tracking score by sesssion";
        TrackingShade.text = "tracking score by sesssion";
        EventSystem.current.SetSelectedGameObject(ButtonBySessionON);
    }

    public void TrackBySessionOFF() //Is On
    {
        ButtonBySessionON.SetActive(false);
        ScoreTracker.instance.TrackBySession = false;
        ButtonBySessionOFF.SetActive(true);
        ScoreTracker.instance.Player1_Score = 0;
        ScoreTracker.instance.Player2_Score = 0;
        TrackingText.text = "tracking score by game";
        TrackingShade.text = "tracking score by game";
        EventSystem.current.SetSelectedGameObject(ButtonBySessionOFF);
    }

    public GameObject CreditsReturn;
    public void Credits()
    {
        credits.SetActive(true);
        menu.SetActive(false);
        EventSystem.current.SetSelectedGameObject(CreditsReturn);
    }

    public GameObject ControlsReturn;
    public void Controls()
    {
        controls.SetActive(true);
        menu.SetActive(false );
        EventSystem.current.SetSelectedGameObject(ControlsReturn);
    }

    public void MusicOn()
    {
        ScoreTracker.instance.Sound = true;
        ScoreTracker.instance.PlayMusic.Post(gameObject);
        SetMusicON.SetActive(false );
        SetMusicOFF.SetActive(true);
        EventSystem.current.SetSelectedGameObject(SetMusicOFF);
    }

    public void MusicOff()
    {
        ScoreTracker.instance.Sound = false;
        StopMusic.Post(gameObject);
        SetMusicON.SetActive(true);
        SetMusicOFF.SetActive(false);
        EventSystem.current.SetSelectedGameObject(SetMusicON);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
