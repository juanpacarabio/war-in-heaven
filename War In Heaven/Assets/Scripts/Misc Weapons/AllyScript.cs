using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyScript : MonoBehaviour
{
    public GameObject player;
    public float fireRate;
    public GameObject instancePoint;
    public GameObject projectile;
    public bool IsTop;
    float internalFireRate;
    public GameObject explosion;
    void Start()
    {
        if (transform.position.x < 0)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, -90f);
        }
        if (transform.position.x > 0)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 90f);
        }
        internalFireRate = fireRate;
        StartCoroutine(Shoot());
    }

    void Update()
    {
        if (IsTop == true)
        {
            transform.position = (player.transform.position + new Vector3(0f, 1f, 0f));
        }
        if (IsTop == false)
        {
            transform.position = (player.transform.position + new Vector3(0f, -1f, 0f));
        }
    }

    public IEnumerator Shoot()
    {
        int a = 0;
        while (a == 0) 
        { 
            yield return new WaitForSeconds(internalFireRate);
            projectile = player.GetComponent<PlayerControl>().currentProjectile;
            if (projectile == player.GetComponent<PlayerControl>().EMP)
            {
                projectile = player.GetComponent<PlayerControl>().baseLaser;
            }
            if (projectile == player.GetComponent<PlayerControl>().grower)
            {
                internalFireRate = 2f;
            }
            if (projectile == player.GetComponent<PlayerControl>().ZigZagger)
            {
                internalFireRate = 0.4f;
            }
            if (projectile == player.GetComponent<PlayerControl>().DoubleZigZagger)
            {
                internalFireRate = 0.6f;
            }
            if (projectile == player.GetComponent<PlayerControl>().grower)
            {
                internalFireRate = 2f;
            }
            if (projectile == player.GetComponent<PlayerControl>().baseLaser)
            {
                internalFireRate = fireRate;
            }
            if (projectile == player.GetComponent<PlayerControl>().tripleshot)
            {
                internalFireRate = fireRate;
            }

            GameObject laserA = Instantiate(projectile, instancePoint.transform.position, instancePoint.transform.rotation);
            laserA.GetComponent<LaserScript>().player = player.gameObject;
        }
    }

    public void GetDestroyed()
    {
        if (IsTop == true)
        {
            player.GetComponent<PlayerControl>().AllyTop = null;
        }
        if (IsTop == false)
        {
            player.GetComponent<PlayerControl>().AllyBot = null;
        }
        Instantiate(explosion, transform.position, transform.rotation);
        Destroy(this.gameObject);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Destroy(collision.gameObject);
            if (IsTop == true)
            {
                player.GetComponent<PlayerControl>().AllyTop = null;
            }
            if (IsTop == false)
            {
                player.GetComponent<PlayerControl>().AllyBot = null;
            }
            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(collision.gameObject);
            if (IsTop == true)
            {
                player.GetComponent<PlayerControl>().AllyTop = null;
            }
            if (IsTop == false)
            {
                player.GetComponent<PlayerControl>().AllyBot = null;
            }
            Destroy(this.gameObject);
        }
    }
}
