using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicScript : MonoBehaviour
{
    public static MusicScript instance = null;
    public AK.Wwise.Event Music;
    public AK.Wwise.Event MidHPMusic;
    public AK.Wwise.Event LowHPMusic;
    public AK.Wwise.Event ResetMusic;
    public AK.Wwise.Event MuteGame;

    GameObject Player1, Player2;
    int P1_HP, P2_HP;
    

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        Music.Post(gameObject);
    }

    private void Start()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        foreach (GameObject p in players)
        {
            if (p.gameObject.name == "Player1")
            {
                Player1 = p;
            }
            if (p.gameObject.name == "Player2")
            {
                Player2 = p;
            }
        }
        if (ScoreTracker.instance.Sound == false)
        {
            MuteGame.Post(gameObject);
        }
    }

    public void UpdateMusic()
    {
        P1_HP = Player1.GetComponent<PlayerControl>().health;
        P2_HP = Player2.GetComponent<PlayerControl>().health;

        if (P2_HP == 3 && P1_HP == 3)
        {
            ResetMusic.Post(gameObject);
        }else if ((P1_HP == 2 && P2_HP != 1) || (P1_HP != 1 && P2_HP == 2) || (P1_HP == 2 && P2_HP == 2))
        {
            MidHPMusic.Post(gameObject);
        }else if (P1_HP == 1 ||  P2_HP == 1 || (P1_HP == 1 && P2_HP == 1))
        {
            LowHPMusic.Post(gameObject);
        }
    }
}
