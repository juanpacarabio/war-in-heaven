using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerControl : MonoBehaviour
{
    public int health = 3;
    public bool canBeHit;
    public float invulnerabilityTime;

    public GameObject player;
    public GameObject ShootingPoint;
    public float speed;
    public GameObject explosion;
    public bool canMoveLeft, canMoveRight, canMoveUp, canMoveDown, canmoveGeneral;

    public InputAction Shooting;
    #region Weapons
    public int Weapon;
    public float WeaponSpeed;
    public GameObject currentProjectile;
    public GameObject baseLaser, tripleshot, shield, piercingShot, grower, EMP, ZigZagger, DoubleZigZagger;
    public bool canShoot;
    public float reloadTime;
    #endregion

    #region UI

    public TMPro.TMP_Text CurrentHP;
    public GameObject shieldImage;

    #endregion

    private void Awake()
    {
        currentProjectile = baseLaser;
    }
    public virtual void Start()
    {
        reloadTime = 0.2f;
        canShoot = true;
        canBeHit = true;
        canmoveGeneral = true;
    }

    public virtual void Update()
    {
        canMoveLeft = !SomethingLeft();
        canMoveRight = !SomethingRight();
        canMoveDown = !SomethingDown();
        canMoveUp = !SomethingUp();
        direction = PlayerMovement.ReadValue<Vector2>();
        if (canmoveGeneral == true)
        {
            Move();
        }
    }

    public void Shoot(InputAction.CallbackContext context)
    {
        if (canShoot == true)
        {
            GameObject laser = Instantiate(currentProjectile, ShootingPoint.transform.position, ShootingPoint.transform.rotation);
            laser.GetComponent<LaserScript>().player = this.gameObject;
            canShoot = false;
            StartCoroutine(Reload());
            if (currentProjectile == EMP)
            {
                ChangeWeapon(0);
            }
        }
    }

    public IEnumerator Reload()
    {
        yield return new WaitForSeconds(reloadTime);
        canShoot = true;
    }

    public void EMPhit()
    {
        canShoot = false;
        StartCoroutine(RebootSystems());
    }
    public IEnumerator RebootSystems()
    {
        yield return new WaitForSeconds(5);
        canShoot = true;
    }

    public void ChangeWeapon(int weapon)
    {
        if(weapon == 0)
        {
            currentProjectile = baseLaser;
            reloadTime = 0.2f;
        }if(weapon == 1)
        {
            currentProjectile = tripleshot;
            reloadTime = 0.2f;
            StartCoroutine(WeaponDuration());
        }if (weapon == 2)
        {
            currentProjectile = piercingShot;
            reloadTime = 0.5f;
            StartCoroutine(WeaponDuration());
        }
        if (weapon == 3)
        {
            currentProjectile = grower;
            reloadTime = 1f;
            StartCoroutine(WeaponDuration());
        }
        if (weapon == 4)
        {
            currentProjectile = EMP;
            reloadTime = 1.5f;
            canShoot = false;
            StartCoroutine(Reload());
        }
        if (weapon == 5)
        {
            currentProjectile = ZigZagger;
            reloadTime = 0.2f;
            StartCoroutine(WeaponDuration());
        }
        if (weapon == 6)
        {
            currentProjectile = DoubleZigZagger;
            reloadTime = 0.4f;
            StartCoroutine(WeaponDuration());
        }
    }

    public IEnumerator WeaponDuration()
    {
        yield return new WaitForSeconds(8);
        ChangeWeapon(0);
    }
    public AK.Wwise.Event DamageEvent;
    public AK.Wwise.Event ShieldHitEvent;
    public void TakeDamage()
    {
        if (canBeHit == true)
        {
            health = health - 1;
            MusicScript.instance.UpdateMusic();
            CurrentHP.text = health.ToString();
            invulnerabilityTime = 1;
            Instantiate(explosion, transform.position, transform.rotation);
            DamageEvent.Post(gameObject);
            if (health <= 0)
            {
                Lose();
            }
            StartCoroutine(Invulnerability());
        }else if (canBeHit == false)
        {
            ShieldHitEvent.Post(gameObject);
        }
    }

    public void Heal()
    {
        if (health < 3)
        {
            health++;
            MusicScript.instance.UpdateMusic();
            CurrentHP.text = health.ToString();
        }
        if (health >= 3)
        {
            OverCharge();
        }
    }

    public void OverCharge()
    {
        if (canBeHit == true)
        {
            invulnerabilityTime = 5;
            StartCoroutine(Invulnerability());
        }
        if (canBeHit == false)
        {
            StopCoroutine(Invulnerability());
            StartCoroutine(Invulnerability());
        }
    }

    public GameObject ForceField;
    public IEnumerator Invulnerability()
    {
        canBeHit = false;
        shieldImage.SetActive(true);
        ForceField.SetActive(true);
        player.GetComponent<Outline>().OutlineColor = new Color32(0, 255, 241, 255);
        yield return new WaitForSeconds(invulnerabilityTime);
        if (player.name == "Player1")
        {
            player.GetComponent<Outline>().OutlineColor = new Color32(0, 24, 255, 255);
        }else if (player.name == "Player2")
        {
            player.GetComponent<Outline>().OutlineColor = new Color32(255, 143, 0, 255);
        }
        shieldImage.SetActive(false);
        ForceField.SetActive(false);
        canBeHit = true;

    }
    public virtual void Lose()
    {
    }

    public GameObject AllyShip;
    public GameObject AllyTop;
    public GameObject AllyBot;

    public void getAllies()
    {

        if (AllyTop == null)
        {
            AllyTop = Instantiate(AllyShip, transform.position + new Vector3(0f, 1f, 0f), transform.rotation);
            AllyTop.GetComponent<AllyScript>().IsTop = true;
            AllyTop.GetComponent<AllyScript>().player = this.gameObject;
        }
        if (AllyTop != null)
        {
            AllyTop.GetComponent<AllyScript>().fireRate = AllyTop.GetComponent<AllyScript>().fireRate / 2;
        }
        if (AllyBot == null)
        {
            AllyBot = Instantiate(AllyShip, transform.position + new Vector3(0f, -1f, 0f), transform.rotation);
            AllyBot.GetComponent<AllyScript>().IsTop = false;
            AllyBot.GetComponent<AllyScript>().player = this.gameObject;
        }
        if (AllyBot != null)
        {
            AllyBot.GetComponent<AllyScript>().fireRate = AllyBot.GetComponent<AllyScript>().fireRate / 2;
        }

    }

    public virtual void Move()
    {
        if ((direction.x == 0 && direction.y > 0) && canMoveUp)
        {
            player.GetComponent<Rigidbody>().velocity = new Vector3(0, 1 * speed, 0);
        }
        else if ((direction.x == 0 && direction.y < 0) && canMoveDown)
        {
            player.GetComponent<Rigidbody>().velocity = new Vector3(0, 1 * -speed, 0);
        }

        else if ((direction.x < 0 && direction.y == 0) && canMoveLeft)
        {
            player.GetComponent<Rigidbody>().velocity = new Vector3(1 * -speed, 0, 0);
        }
        else if ((direction.x > 0 && direction.y == 0) && canMoveRight)
        {
            player.GetComponent<Rigidbody>().velocity = new Vector3(1 * speed, 0, 0);
        }
        else if (direction.x != 0 && direction.y != 0)
        {
            if ((direction.y > 0) && canMoveUp)
            {
                player.GetComponent<Rigidbody>().velocity = new Vector3(0, 1 * speed, 0);
            }
            else if ((direction.y < 0) && canMoveDown)
            {
                player.GetComponent<Rigidbody>().velocity = new Vector3(0, 1 * -speed, 0);
            }
        }

        else
        {
            player.GetComponent<Rigidbody>().velocity = Vector3.zero;
            player.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
    }

    public GameObject Heart;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameObject kiss = Instantiate(Heart, ShootingPoint.transform.position + new Vector3(0f, Random.Range(0.5f,1f), 0f), ShootingPoint.transform.rotation);
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameObject kiss = Instantiate(Heart, ShootingPoint.transform.position + new Vector3(0f, Random.Range(0.5f, 1f), 0f), ShootingPoint.transform.rotation);
        }
    }

    #region MOVEMENT-CHECKS
    internal RaycastHit downHit, upHit, leftHit, rightHit;
    public virtual bool SomethingDown()
    {
        Ray landingRay = new Ray(new Vector3(transform.position.x, transform.position.y - (transform.localScale.y / 2.2f), transform.position.z), Vector3.down);
        Debug.DrawRay(landingRay.origin, landingRay.direction, Color.green);
        return Physics.Raycast(landingRay, out downHit, transform.localScale.x / 1.8f);
    }
    public virtual bool SomethingUp()
    {
        Ray landingRay = new Ray(new Vector3(transform.position.x, transform.position.y - (transform.localScale.y / -3.2f), transform.position.z), Vector3.up);
        Debug.DrawRay(landingRay.origin, landingRay.direction, Color.green);
        return Physics.Raycast(landingRay, out upHit, transform.localScale.x / 1.8f);
    }
    public virtual bool SomethingRight()
    {
        Ray landingRay = new Ray(new Vector3(transform.position.x, transform.position.y - (transform.localScale.y / 2.2f), transform.position.z), Vector3.right);
        Debug.DrawRay(landingRay.origin, landingRay.direction, Color.green);
        return Physics.Raycast(landingRay, out rightHit, transform.localScale.x / 1.8f);
    }
    public virtual bool SomethingLeft()
    {
        Ray landingRay = new Ray(new Vector3(transform.position.x, transform.position.y - (transform.localScale.y / 2.2f), transform.position.z), Vector3.left);
        Debug.DrawRay(landingRay.origin, landingRay.direction, Color.green);
        return Physics.Raycast(landingRay, out leftHit, transform.localScale.x / 1.8f);
    }
    #endregion


    public Vector2 direction = Vector2.zero;
    public InputAction PlayerMovement;

    private void OnEnable()
    {
        PlayerMovement.Enable();
        Shooting.Enable();
        Shooting.performed += Shoot;
    }

    private void OnDisable()
    {
        PlayerMovement.Disable();
        Shooting.Disable();
    }
}
