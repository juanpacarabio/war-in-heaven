using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    private void Update()
    {
        transform.Rotate(Time.deltaTime * 150, Time.deltaTime * 150, Time.deltaTime * 150, Space.Self);
    }
}
