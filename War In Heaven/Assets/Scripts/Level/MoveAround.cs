using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAround : MonoBehaviour
{
    public GameObject[] objectives;
    public float speed;
    int objective;
    int previousObjective;
    float Radius = 1;

    private void Start()
    {
        ChangeObjective();
    }
    private void Update()
    {
        if(Vector3.Distance(objectives[objective].transform.position, transform.position) < Radius)
        {
            ChangeObjective();
        }

        transform.position = Vector3.MoveTowards(transform.position, objectives[objective].transform.position, Time.deltaTime * speed);

        if (GameManager.instance.GameIsOver == true)
        {
            Destroy(this.gameObject.GetComponent<MoveAround>());
        }
    }
    void ChangeObjective()
    {
        previousObjective = objective;
        objective = Random.Range(0, objectives.Length);

        if (objective == previousObjective)
        {
            ChangeObjective();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerControl>().TakeDamage();
        }
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Destroy(collision.gameObject);
        }
    }
}
