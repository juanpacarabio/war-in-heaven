using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingWallInstanciator : MonoBehaviour
{
    public GameObject instancePoint;
    public GameObject MaxDistancePoint;
    public GameObject MinDistancePoint;
    Vector3 position = new Vector3();
    float spawnCooldown;
    public float MinSpawnTime, MaxSpawnTime;
    public GameObject SpawnWall;
    void Start()
    {
        StartCoroutine(SpawnFallingWall());
    }

    void Update()
    {
        position = new Vector3(Mathf.Lerp(MinDistancePoint.transform.position.x, MaxDistancePoint.transform.position.x, Mathf.PingPong(Time.time * 0.5f, 1)), instancePoint.transform.position.y, instancePoint.transform.position.z);
        instancePoint.transform.position = position;
    }

    public IEnumerator SpawnFallingWall()
    {
        bool a = true;
        while (a == true)
        {
            Instantiate(SpawnWall, instancePoint.transform.position, instancePoint.transform.rotation);
            spawnCooldown = Random.Range(MinSpawnTime, MaxSpawnTime);
            yield return new WaitForSeconds(spawnCooldown);
        }
    }
}
