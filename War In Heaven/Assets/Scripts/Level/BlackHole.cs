using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour
{
    public GameObject InstancePoint;
    public GameObject[] Enemies;
    public float MinSpawnTime, MaxSpawnTime;
    void Start()
    {
        StartCoroutine(SpawnEnemy());
    }

    
    void Update()
    {
        transform.Rotate(0f, 0f, Time.deltaTime * 150, Space.Self);
    }

    float spawnCooldown;
    public IEnumerator SpawnEnemy()
    {
        yield return new WaitForSeconds(2);
        bool active = true;
        while (active == true)
        {
            int enemy = Random.Range(0, Enemies.Length);
            Instantiate(Enemies[enemy], InstancePoint.transform.position, InstancePoint.transform.rotation);
            spawnCooldown = Random.Range(MinSpawnTime, MaxSpawnTime);
            yield return new WaitForSeconds(spawnCooldown);
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerControl>().TakeDamage();
        }
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Destroy(collision.gameObject);
        }
    }

}
