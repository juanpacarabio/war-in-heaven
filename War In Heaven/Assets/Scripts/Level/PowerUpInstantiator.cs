using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpInstantiator : MonoBehaviour
{
    
    public GameObject instancePoint;
    public GameObject MaxDistancePoint;
    public GameObject MinDistancePoint;
    Vector3 position = new Vector3();

    GameObject selectedPowerUp;
    float spawnCooldown;
    public float MinSpawnTime, MaxSpawnTime;
    public GameObject[] PowerUps;
    void Start()
    {
        StartCoroutine(SpawnPowerUp());
    }


    void Update()
    {
        position = new Vector3(Mathf.Lerp(MinDistancePoint.transform.position.x, MaxDistancePoint.transform.position.x, Mathf.PingPong(Time.time * 0.5f, 1)), instancePoint.transform.position.y, instancePoint.transform.position.z);
        instancePoint.transform.position = position;
    }

    public IEnumerator SpawnPowerUp()
    {
        bool a = true;
        while (a == true)
        {
            SelectPowerUp();
            Instantiate(selectedPowerUp, instancePoint.transform.position, instancePoint.transform.rotation);
            spawnCooldown = Random.Range(MinSpawnTime, MaxSpawnTime);
            yield return new WaitForSeconds(spawnCooldown);
        }
    }

    void SelectPowerUp()
    {
        int powerUp = Random.Range(0, PowerUps.Length);
        selectedPowerUp = PowerUps[powerUp];
    }
}
