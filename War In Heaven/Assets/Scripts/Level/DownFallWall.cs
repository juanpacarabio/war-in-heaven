using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownFallWall : MonoBehaviour
{
    public GameObject explosion;
    private void Start()
    {
        transform.localScale = transform.localScale + new Vector3(0f, Random.Range(0.5f, 3f), 0);
    }
    public virtual void Update()
    {
        transform.Translate(0f, -Time.deltaTime, 0f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerControl>().TakeDamage();
        }
        if (collision.gameObject.CompareTag("Ally"))
        {
            collision.gameObject.GetComponent<AllyScript>().GetDestroyed();
        }
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Destroy(collision.gameObject);
            if (collision.gameObject.name.Substring(0, 3) == "EMP")
            {
                Instantiate(explosion, transform.position, transform.rotation);
                Destroy(this.gameObject);
            }
        }
    }
}
