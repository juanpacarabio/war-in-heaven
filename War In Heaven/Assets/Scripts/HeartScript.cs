using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartScript : MonoBehaviour
{
    float DestroyTime = 2;
    private void Update()
    {
        transform.Translate(0f, -Time.deltaTime, 0f);
        DestroyTime = DestroyTime - Time.deltaTime;
        if (DestroyTime <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
