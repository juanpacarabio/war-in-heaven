using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombClear_PowerUp : BasicPowerup
{
    public GameObject explosion;
    public override void Start()
    {
        base.Start();
        isWeapon = false;
    }

    public override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            ClearScreen();
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            ClearScreen();
        }
            PickedUpEvent.Post(gameObject);
        Destroy(this.gameObject);
    }

    void ClearScreen()
    {
        GameObject[] projectiles = GameObject.FindGameObjectsWithTag("Projectile");
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        GameObject[] powerUps = GameObject.FindGameObjectsWithTag("PowerUp");

        foreach (GameObject destroyable in projectiles)
        {
            Instantiate(explosion, destroyable.transform.position, destroyable.transform.rotation);
            Destroy(destroyable);
        }
        foreach (GameObject destroyable in enemies)
        {
            Instantiate(explosion, destroyable.transform.position, destroyable.transform.rotation);
            Destroy(destroyable);
        }
        foreach (GameObject destroyable in powerUps)
        {
            Instantiate(explosion, destroyable.transform.position, destroyable.transform.rotation);
            Destroy(destroyable);
        }
    }
}
