using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grower_PowerUp : BasicPowerup
{
    public override void Start()
    {
        base.Start();
        weaponId = 3;
        isWeapon = true;
    }
}
