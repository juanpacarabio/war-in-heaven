using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TripleShot_PowerUp : BasicPowerup
{
    public override void Start()
    {
        base.Start();
        weaponId = 1;
        isWeapon = true;
    }
}
