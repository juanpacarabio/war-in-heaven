using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleZigZagger_PowerUp : BasicPowerup
{
    public override void Start()
    {
        base.Start();
        weaponId = 6;
        isWeapon = true;
    }
}
