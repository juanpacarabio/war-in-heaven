using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Overcharge_PowerUp : BasicPowerup
{
    public override void Start()
    {
        base.Start();
        isWeapon = false;
    }

    public override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            player = collision.gameObject.GetComponent<LaserScript>().player;
            player.GetComponent<PlayerControl>().OverCharge();
            if (player != null)
            {
                PickedUpEvent.Post(gameObject);
                Debug.Log("Deber�a cambiar arma");
            }
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            player = collision.gameObject;
            player.GetComponent<PlayerControl>().OverCharge();
            PickedUpEvent.Post(gameObject);
        }
        if (collision.gameObject.CompareTag("Ally"))
        {
            player = collision.gameObject.GetComponent<AllyScript>().player;
            player.GetComponent<PlayerControl>().OverCharge();
            PickedUpEvent.Post(gameObject);
        }
        Destroy(this.gameObject);
    }
}
