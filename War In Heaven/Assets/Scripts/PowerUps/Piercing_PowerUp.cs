using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piercing_PowerUp : BasicPowerup
{
    public override void Start()
    {
        base.Start();
        weaponId = 2;
        isWeapon = true;
    }
}
