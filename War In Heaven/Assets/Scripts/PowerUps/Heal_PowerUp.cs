using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal_PowerUp : BasicPowerup
{
    public override void Start()
    {
        base.Start();
        isWeapon = false;
    }

    public override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            player = collision.gameObject.GetComponent<LaserScript>().player;
            player.GetComponent<PlayerControl>().Heal();
            player = collision.gameObject.GetComponent<LaserScript>().player;
            if (player != null)
            {
                PickedUpEvent.Post(gameObject);
                Debug.Log("Deber�a cambiar arma");
            }
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            player = collision.gameObject;
            player.GetComponent<PlayerControl>().Heal();
            PickedUpEvent.Post(gameObject);
        }
        Destroy(this.gameObject);
    }
}
