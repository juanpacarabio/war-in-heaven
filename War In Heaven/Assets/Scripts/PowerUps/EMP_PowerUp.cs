using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EMP_PowerUp : BasicPowerup
{
    public override void Start()
    {
        base.Start();
        weaponId = 4;
        isWeapon = true;
    }
}
