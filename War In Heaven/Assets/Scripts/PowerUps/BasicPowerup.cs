using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicPowerup : MonoBehaviour
{
    public GameObject player;
    public bool isWeapon;
    public int weaponId;
    public AK.Wwise.Event PickedUpEvent;
    public virtual void Start()
    {
        
    }

    public virtual void Update()
    {
        transform.Translate(0f, -Time.deltaTime, 0f);
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            player = collision.gameObject.GetComponent<LaserScript>().player;

            if (player != null)
            {
                if (isWeapon == true)
                {
                    player.GetComponent<PlayerControl>().ChangeWeapon(weaponId);
                    if (player != null)
                    {
                        PickedUpEvent.Post(gameObject);
                        Debug.Log("Deber�a cambiar arma");
                    }
                }
            }
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            player = collision.gameObject;
            if (isWeapon == true)
            {
                player.GetComponent<PlayerControl>().ChangeWeapon(weaponId);
                if (player != null)
                {
                    PickedUpEvent.Post(gameObject);
                    Debug.Log("Deber�a cambiar arma");
                }
            }
        }
        if (collision.gameObject.CompareTag("Ally"))
        {
            player = collision.gameObject.GetComponent<AllyScript>().player;
            if (isWeapon == true)
            {
                player.GetComponent<PlayerControl>().ChangeWeapon(weaponId);
                if (player != null)
                {
                    PickedUpEvent.Post(gameObject);
                    Debug.Log("Deber�a cambiar arma");
                }
            }
        }
        Destroy(this.gameObject);
    }
}
