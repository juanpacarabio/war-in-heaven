using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZigZagger_PowerUp : BasicPowerup
{
    public override void Start()
    {
        base.Start();
        weaponId = 5;
        isWeapon = true;
    }
}
