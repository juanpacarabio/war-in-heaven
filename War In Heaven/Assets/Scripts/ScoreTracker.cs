using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreTracker : MonoBehaviour
{
    public static ScoreTracker instance = null;
    public int Player1_Score, Player2_Score;
    public bool TrackBySession = false;
    public int P1_Kills, P2_Kills;

    public bool Sound;
    public AK.Wwise.Event StopMusic;
    public AK.Wwise.Event PlayMusic;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
        Sound = true;
    }
}
