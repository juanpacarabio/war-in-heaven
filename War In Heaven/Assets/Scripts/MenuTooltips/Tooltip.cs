using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour
{
    public string message;

    private void OnMouseEnter()
    {
        TooltipManager.instance.ShowTooltip(message);
    }

    private void OnMouseExit()
    {
        TooltipManager.instance.HideTooltip();
    }
    private void OnMouseOver()
    {
        TooltipManager.instance.ShowTooltip(message);
    }
}
