Event	ID	Name			Wwise Object Path	Notes
	367021569	PlayMenuTheme			\Default Work Unit\PlayMenuTheme	
	391172934	EnemyHit			\Default Work Unit\EnemyHit	
	1104796412	SniperShot			\Default Work Unit\SniperShot	
	1287522420	MuteGame			\Default Work Unit\MuteGame	
	1893297113	GoToMenu			\Default Work Unit\GoToMenu	
	2513197477	ShieldHit			\Default Work Unit\ShieldHit	
	2577391249	PlayersOn3HP			\Default Work Unit\PlayersOn3HP	
	3395060318	PlayerOn1HP			\Default Work Unit\PlayerOn1HP	
	3427604237	BigShot			\Default Work Unit\BigShot	
	3562689317	PlayerOn2HP			\Default Work Unit\PlayerOn2HP	
	3831688773	PlayerHit			\Default Work Unit\PlayerHit	
	3950429679	PowerUp			\Default Work Unit\PowerUp	
	3991942870	Music			\Default Work Unit\Music	
	4158285989	GameOver			\Default Work Unit\GameOver	
	4186217887	StopMenuTheme			\Default Work Unit\StopMenuTheme	
	4215250317	BasicShot			\Default Work Unit\BasicShot	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	139855978	WIH_GameOver	D:\Repos\war-in-heaven\War In Heaven\War In Heaven_WwiseProject\.cache\Mac\SFX\WIH_GameOver_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\WIH_GameOver		723620
	144601302	WIH_MenuMusic	D:\Repos\war-in-heaven\War In Heaven\War In Heaven_WwiseProject\.cache\Mac\SFX\WIH_MenuMusic_D79FB2B7.wem		\Actor-Mixer Hierarchy\Default Work Unit\WIH_MenuMusic		24196892
	277975785	WIH_PlayerHit3	D:\Repos\war-in-heaven\War In Heaven\War In Heaven_WwiseProject\.cache\Mac\SFX\WIH_PlayerHit3_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\PlayerHit\WIH_PlayerHit3		656824
	426151892	WIH_EnemyHit3	D:\Repos\war-in-heaven\War In Heaven\War In Heaven_WwiseProject\.cache\Mac\SFX\WIH_EnemyHit3_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\EnemyHit\WIH_EnemyHit3		439968
	486298035	WIH_PlayerHit2	D:\Repos\war-in-heaven\War In Heaven\War In Heaven_WwiseProject\.cache\Mac\SFX\WIH_PlayerHit2_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\PlayerHit\WIH_PlayerHit2		305440
	560953923	WIH_BigShot	D:\Repos\war-in-heaven\War In Heaven\War In Heaven_WwiseProject\.cache\Mac\SFX\WIH_BigShot_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\WIH_BigShot		88296
	609596031	WIH_PowerUp	D:\Repos\war-in-heaven\War In Heaven\War In Heaven_WwiseProject\.cache\Mac\SFX\WIH_PowerUp_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\WIH_PowerUp		239292
	714519658	WIH_BasicShot	D:\Repos\war-in-heaven\War In Heaven\War In Heaven_WwiseProject\.cache\Mac\SFX\WIH_BasicShot_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\WIH_BasicShot		88296
	715382819	WIH_Fireworks	D:\Repos\war-in-heaven\War In Heaven\War In Heaven_WwiseProject\.cache\Mac\SFX\WIH_Fireworks_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\WIH_Fireworks		402580
	734739708	WIH_PlayerHit	D:\Repos\war-in-heaven\War In Heaven\War In Heaven_WwiseProject\.cache\Mac\SFX\WIH_PlayerHit_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\PlayerHit\WIH_PlayerHit		458404
	779234706	WIH_ShieldHit	D:\Repos\war-in-heaven\War In Heaven\War In Heaven_WwiseProject\.cache\Mac\SFX\WIH_ShieldHit_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\WIH_ShieldHit		355696
	832355792	WIH_EnemyHit	D:\Repos\war-in-heaven\War In Heaven\War In Heaven_WwiseProject\.cache\Mac\SFX\WIH_EnemyHit_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\EnemyHit\WIH_EnemyHit		540620
	862916306	WIH_SniperShot	D:\Repos\war-in-heaven\War In Heaven\War In Heaven_WwiseProject\.cache\Mac\SFX\WIH_SniperShot_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\WIH_SniperShot		88296
	884083477	WIH_EnemyHit2	D:\Repos\war-in-heaven\War In Heaven\War In Heaven_WwiseProject\.cache\Mac\SFX\WIH_EnemyHit2_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\EnemyHit\WIH_EnemyHit2		574264
	964835725	WIHBackground Music	D:\Repos\war-in-heaven\War In Heaven\War In Heaven_WwiseProject\.cache\Mac\SFX\WIHBackground Music_A73F1CA0.wem		\Actor-Mixer Hierarchy\Default Work Unit\WIHBackground Music		24184584

